package com.hopitalityapp.distributedcache.persistence.dao;

import com.datastax.driver.core.*;
import com.hazelcast.core.HazelcastJsonValue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Slf4j
public class PotusDAO {

    private PreparedStatement loadStatement;
    private PreparedStatement insertStatement;
    private PreparedStatement loadAllKeyStatement;

    private Session cassandraSession;

    @Autowired
    public PotusDAO(Session cassandraSession) {
        this.cassandraSession = cassandraSession;

        loadAllKeyStatement = this.cassandraSession.prepare("SELECT id FROM potus;");
        loadStatement = this.cassandraSession.prepare("SELECT * from potus where id = ? ;")
                .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM);
        insertStatement = this.cassandraSession.prepare(
                        "INSERT INTO potus (id, json_value) VALUES (?,?);")
                .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM);
    }

    public void save(String id, HazelcastJsonValue hazelcastJsonValue){
        BoundStatement boundStatement = insertStatement.bind(id, hazelcastJsonValue.toString());
        cassandraSession.execute(boundStatement);
    }

    public HazelcastJsonValue load(String id){

        HazelcastJsonValue hazelcastJsonValue = null;

        BoundStatement boundStatement = loadStatement.bind(id);
        ResultSet resultSet = cassandraSession.execute(boundStatement);

        Row row = resultSet.one();
        if(row != null){
            hazelcastJsonValue = new HazelcastJsonValue(row.getString("json_value"));
        }

        return hazelcastJsonValue;
    }

    public void saveAll(Map<String, HazelcastJsonValue> hazelcastJsonValueMap){
        hazelcastJsonValueMap.entrySet().stream().forEach(entry -> save(entry.getKey(), entry.getValue()));
    }

    public Map<String, HazelcastJsonValue> loadAll(Collection<String> keys){

        Map<String,HazelcastJsonValue> hazelcastJsonValueMap = new HashMap<>();
        keys.stream().forEach(key -> hazelcastJsonValueMap.put(key, load(key)));

        return hazelcastJsonValueMap;
    }

    public Collection<String> loadAllKeys(){

        List<String> results = new ArrayList<>();

        ResultSet resultSet = cassandraSession.execute(loadAllKeyStatement.getQueryString());
        Iterator<Row> rowIterator = resultSet.iterator();
        while (rowIterator.hasNext()){
            String id = rowIterator.next().getString("json_value");
            results.add(id);
        }

        return results;

    }


}
