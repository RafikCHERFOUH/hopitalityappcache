package com.hopitalityapp.distributedcache.persistence.repositories;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.hopitalityapp.distributedcache.persistence.domain.Citizen;
import com.hopitalityapp.distributedcache.persistence.domain.User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.datastax.driver.core.querybuilder.QueryBuilder.select;

@Repository
public class CitizenRepository {

    private Mapper<Citizen> mapper;
    private Session session;

    private static final String TABLE = "citizen";

    public CitizenRepository(MappingManager mappingManager) {
        createTable(mappingManager.getSession());
        this.mapper = mappingManager.mapper(Citizen.class);
        this.session = mappingManager.getSession();
    }

    private void createTable(Session session) {
        // use SchemaBuilder to create table
    }

    public Citizen find(String name) {
        return mapper.get(name);
    }

    public List<Citizen> findAll() {
        final ResultSet result = session.execute(select().all().from(TABLE));
        return mapper.map(result).all();
    }

    public List<String> findAllKeys() {
        List<String> keys = new ArrayList<>();
        final ResultSet result = session.execute(select().column("name").from(TABLE));
        result.forEach(row -> keys.add(row.getString("name")));
        return keys;
    }

    public List<Citizen> findAllByName(String name) {
        final ResultSet result = session.execute(select().all().from(TABLE).where(eq("name", name)));
        return mapper.map(result).all();
    }

    public void delete(String name) {
        mapper.delete(name);
    }

    public Citizen save(Citizen citizen) {
        mapper.save(citizen);
        return citizen;
    }
}
