package com.hopitalityapp.distributedcache.persistence.domain;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.Data;

@Data
@Table(name = "citizen")
public class Citizen {

    @PartitionKey
    private String name;

    private int favouriteNumber;

    private String favouriteColor;

    public Citizen(String name, int favouriteNumber, String favouriteColor) {
        this.name = name;
        this.favouriteNumber = favouriteNumber;
        this.favouriteColor = favouriteColor;
    }

    public Citizen() {
    }
}
