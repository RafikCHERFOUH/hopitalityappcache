package com.hopitalityapp.distributedcache.persistence.repositories;

import com.datastax.driver.core.*;
import com.hopitalityapp.distributedcache.persistence.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class UserRepository {

    protected PreparedStatement insertStatement;
    protected PreparedStatement loadStatement;
    protected PreparedStatement loadAllKeysStatement;

    @Autowired
    private Session cassandraSession;

    @PostConstruct
    public void configure() {
        insertStatement = this.cassandraSession.prepare(
                "INSERT INTO USER (name, favourite_color, favourite_number) VALUES (?, ?, ?);")
                .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM);
        String queryLoadOne = "SELECT * FROM USER WHERE NAME = ? ;";
        loadStatement = this.cassandraSession.prepare(queryLoadOne)
                .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM);
        String queryLoadAllKeys = "SELECT NAME FROM USER";
        loadAllKeysStatement = this.cassandraSession.prepare(queryLoadAllKeys)
                .setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM);
    }

    public void save(User user){
        BoundStatement boundStatement;
        boundStatement = insertStatement(insertStatement, user);
        cassandraSession.execute(boundStatement);
    }

    public User load(String name){

        User user = null;

        ResultSet resultSet = cassandraSession.execute(loadStatement.bind(name));
        Row row = resultSet.one();

        if(row != null)
            user = User.newBuilder().setName(row.getString("name")).setFavoriteColor(row.getString("favourite_color")).setFavoriteNumber(row.getInt("favourite_number")).build();

        return user;

    }

    public Set<String> loadAllKeys() {
        ResultSet resultSet = cassandraSession.execute(loadAllKeysStatement.bind());
        return resultSet.all().stream().filter(row -> row != null).map(row -> row.getString("name")).collect(Collectors.toSet());
    }

    protected BoundStatement insertStatement(PreparedStatement statement, User user) {
        return statement.bind(user.getName().toString(), user.getFavoriteColor().toString(), user.getFavoriteNumber());
    }


}