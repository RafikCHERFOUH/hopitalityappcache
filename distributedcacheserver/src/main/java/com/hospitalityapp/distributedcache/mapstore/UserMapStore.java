package com.hospitalityapp.distributedcache.mapstore;

import com.hazelcast.core.MapStore;
import com.hopitalityapp.distributedcache.persistence.domain.User;
import com.hopitalityapp.distributedcache.persistence.repositories.UserRepository;
import com.hospitalityapp.distributedcache.config.HospitalityAppSpringContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

//@Component
public class UserMapStore implements MapStore<String, User> {

    @Override
    public void store(String s, User user) {
        HospitalityAppSpringContext.userRepository.save(user);
    }

    @Override
    public void storeAll(Map<String, User> map) {
        map.values().stream().forEach(user -> HospitalityAppSpringContext.userRepository.save(user));
    }

    @Override
    public void delete(String name) {

    }

    @Override
    public void deleteAll(Collection<String> collection) {

    }

    @Override
    public User load(String name) {
        return HospitalityAppSpringContext.userRepository.load(name);
    }

    @Override
    public Map<String, User> loadAll(Collection<String> collection) {
        Map<String, User> users = new HashMap<>();
        collection.stream().forEach(str -> users.put(str, HospitalityAppSpringContext.userRepository.load(str)));

        return users;
    }

    @Override
    public Iterable<String> loadAllKeys() {
        return HospitalityAppSpringContext.userRepository.loadAllKeys();
    }
}
