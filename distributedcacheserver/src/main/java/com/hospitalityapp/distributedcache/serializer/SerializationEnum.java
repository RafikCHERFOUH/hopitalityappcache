package com.hospitalityapp.distributedcache.serializer;

import com.hopitalityapp.distributedcache.persistence.domain.User;
import com.hospitalityapp.distributedcache.domain.Person;

public enum SerializationEnum {

    USER(User.class,51),
    PERSON(Person.class, 52),

    ;

    final int typeId;
    final Class clazz;

    SerializationEnum(final Class clazz ,final int typeId) {
        this.typeId = typeId;
        this.clazz = clazz;
    }

    public int getTypeId() {
        return typeId;
    }

    public Class getClazz() {
        return clazz;
    }
}
