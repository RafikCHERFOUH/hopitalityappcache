package launch;

import com.hospitalityapp.distributedcache.config.HazelcastXMLConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Slf4j
@Import(HazelcastXMLConfig.class)
public class HospitalityApplication {

    public static void main(String[] args) {
        SpringApplication.run(HospitalityApplication.class, args);
    }
}
