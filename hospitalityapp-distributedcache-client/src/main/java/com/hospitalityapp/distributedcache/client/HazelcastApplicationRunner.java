package com.hospitalityapp.distributedcache.client;

import com.hazelcast.core.DistributedObject;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastJsonValue;
import com.hazelcast.core.IMap;
import com.hopitalityapp.distributedcache.persistence.domain.Citizen;
import com.hopitalityapp.distributedcache.persistence.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.util.Collection;
import java.util.stream.Collectors;

@Configuration
public class HazelcastApplicationRunner implements CommandLineRunner {

    private static final String POTUS_IMAP_NAME = "POTUS";
    private static final String VPOTUS_IMAP_NAME = "VPOTUS";

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        /*testIfIMapExists("potus");

        HazelcastJsonValue hazelcastJsonValue = new HazelcastJsonValue("{ \"FIRSTNAME\" : \"John\", \"MIDDLENAME1\" : \"null\", \"MIDDLENAME2\" : \"null\", \"LASTNAME\" : \"Adams\", \"TOOKOFFICE\" : \"1797-03-04\", \"LEFTOFFICE\" : \"1801-03-04\", \"AKA\" : \"null\" }\n");
        IMap<String, HazelcastJsonValue> jsonValueIMap = this.hazelcastInstance.getMap("potus");
        jsonValueIMap.put("2", hazelcastJsonValue);*/

        IMap<String, User> users = this.hazelcastInstance.getMap("User");
        User user = User.newBuilder().setName("salem").setFavoriteColor("red").setFavoriteNumber(42).build();
        users.put("salem", user);

        User myUser = users.get("salem");
        System.out.println(myUser.toString() + "\n");

        /*IMap<String, Citizen> citizenIMap = this.hazelcastInstance.getMap("citizen");
        Citizen citizen = new Citizen("ali", 99, "purple");
        citizenIMap.put("ali", citizen);

        Citizen myCitizen = citizenIMap.get("ali");
        System.out.println(myCitizen.toString() + "\n");*/



        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }

    private boolean testIfIMapExists(String iMapName) {

        Collection<DistributedObject> results =
                this.hazelcastInstance.getDistributedObjects()
                        .stream()
                        .filter(distributedObject -> distributedObject.getName().equals(iMapName))
                        .filter(distributedObject -> distributedObject instanceof IMap)
                        .collect(Collectors.toSet());

        return results.size() == 1;
    }
}
